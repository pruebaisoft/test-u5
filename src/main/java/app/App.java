package app;

import app.util.Wine;
import app.model.Network;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import java.io.IOException;
import java.util.logging.Level;


public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws IOException {
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        logger.info((Marker)Level.INFO,"{} correct of {} = {}%",results.correct, results.trials, results.percentage * 100);
    }
}
